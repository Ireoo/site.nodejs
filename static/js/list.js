/**
 * Created by Administrator on 2016/8/30.
 */

$(function() {

    $("form.search input").focus(function() {
        $("form.search").addClass('focus');
    }).blur(function() {
        $("form.search").removeClass('focus');
    });

    $('ul.menu li a').click(function() {
        $('ul.list li').remove();
        load = false;
        n = 0;
        m = $(this).attr('id');
        $('ul.menu li a').removeClass('on');
        $(this).addClass('on');
        var load_div = $('<li />').addClass('center').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>加载中...');
        addList(load_div);
    });

    $(window).scroll(function() {
        // console.log($("body").height() + "   ---    " + $(window).scrollTop() + "   ---   " + document.documentElement.clientHeight + "   ---   " + ($("body").height() - $(window).scrollTop()));
        if (($("body").height() - $(window).scrollTop()) <= document.documentElement.clientHeight + 100 && !load) {
            var load_div = $('<li />').addClass('center').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>加载中...');
            addList(load_div);
        }
    });
});

function addList(div) {
    $.ajax({
        type: "GET",
        url: "/action/list",
        data: { now: n, mode: m, tag: t, keyword: k, timer: timer },
        dataType: "json",
        success: function(res) {
            console.log(res);
            div.remove();
            if (res.code == 1 && res.data.length != 0) {
                timer = res.data[res.data.length - 1].timer;
                load = false;
                n++;
                const today = new Date().getTime();
                $.each(res.data, function(i, e) {
                    var li = $('<li />').appendTo($('ul.list'));
                    var h1 = $('<h1 />').appendTo(li);
                    if ((new Date(e.timer)).format('MM-dd') === (new Date(today)).format('MM-dd')) { h1.before($('<span>最新</span>')); }
                    var a = $('<a />').attr('href', '/' + e._id).attr('target', '_blank').text(e.title).appendTo(h1);
                    var div = $('<div />').addClass('r').appendTo(li);
                    $('<em><i class="fa fa-calendar" aria-hidden="true"></i><b>' + (new Date(e.timer)).format('MM-') + '</b>' + (new Date(e.timer)).format('dd') + '</em>').appendTo(div);
                    $('<em><i class="fa fa-globe" aria-hidden="true"></i>' + (e.count || 0) + '</em>').appendTo(div);
                });
            } else {
                var li = $('<li />').text('没有更多了...').addClass('center').appendTo($('ul.list'));
            }
        },
        beforeSend: function() {
            load = true;
            $('ul.list').append(div);
        },
        complete: function() {},
        error: function(req, status, err) {

        }
    });
}

Date.prototype.format = function(format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};