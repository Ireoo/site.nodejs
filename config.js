exports = module.exports = {
    init: {
        domain: "",
        port: process.env.PORT || 2012
    },
    site: {
        title: process.env.TITLE || "",
        keywords: process.env.KEYWORDS || "",
        description: process.env.DESCRIPTION || ""
    },
    db: {
        url: process.env.API || 'http://api.com/', //使用nodejs编写的数据库接口文件,数据是非关系型的mongodb,具体项目地址:https://github.com/Ireoo/api.mongodb.nodejs
        key: process.env.KEY || 'test',
        table: 'av'
    },
    menu: [{
        name: 'torrent',
        title: 'BT资源'
    }, {
        name: 'image',
        title: '美图欣赏'
    }, {
        name: 'video',
        title: '在线视频'
    }]
};