const express = require('express');
const router = express.Router();

exports = module.exports = router;

router.get('/', (req, res, next) => {
	req.session.userId = '';
    res.redirect(301, '/');
});