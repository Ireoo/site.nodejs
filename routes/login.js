require('./init');
const express = require('express');
const router = express.Router();
const iapi = require('iapi');
const config = require('../config');
const md5 = require('md5');

iapi.config = config.db;
router.data = {};

exports = module.exports = router;

/**
 * 用户登陆
 */
router.get('/', (req, res, next) => {
    res.render('login', { site: config.site, init: { uri: req.query.uri }, user: null });
});

router.post('/', (req, res, next) => {
    let user = req.body;
    iapi.api('user/findone', { where: { username: user.username } }, (err, data) => {
        if (!err) {
            if (md5(user.password) === data.password) {
                delete data.password;
                req.session.userId = data._id;
                ('uri' in user && user.uri !== '') ? res.redirect(301, user.uri): res.redirect(301, '/');
            } else {
                res.render('login', { site: config.site, init: { success: false, err: '密码错误!' }, user: null });
            }
        }
    });
});
