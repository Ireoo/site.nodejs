require('./init');
const express = require('express');
const router = express.Router();
const config = require('../config');

exports = module.exports = router;

router.get('/', (req, res, next) => {
    console.log(req.body, req.params, req.query);
    res.status(200).send({ body: req.body, params: req.params, query: req.query });
});