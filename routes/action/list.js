const iapi = require('iapi');
const config = require('../../config');

iapi.config = config.db;

exports = module.exports = (req, res, next) => {
    console.log(req.query);
    if (req.query.now && req.query.mode && parseInt(req.query.now) > 0 && req.query.mode !== '') {
        let now = parseInt(req.query.now) * 20,
            keyword = req.query.keyword,
            where = { close: { $exists: false } };
        if (('timer' in req.query) && parseInt(req.query.timer) > 0) where.timer = { $lt: parseInt(req.query.timer) };
        if (req.query.tag) where.tags = { $in: [req.query.tag] };
        switch (req.query.mode) {
            // case 'search':
            //     iapi.api('av/find', { where: { title: { $regex: keyword } }, other: { skip: now, limit: 20, sort: { count: -1 } } }, (err, data) => {
            //         if (!err) {
            //             res.status(200).send({ code: 1, data: data });
            //         } else {
            //             console.error(err);
            //             res.status(404).send('系统错误,请稍后再试!');
            //         }
            //     });
            //     break;

            case 'hot':
                if (keyword) where.title = { $regex: keyword };
                iapi.api('av/find', { where: where, other: { skip: now, limit: 20, sort: { count: -1, _id: -1 }, show: { _id: 12, title: 1, timer: 1, count: 1 } } }, (err, data) => {
                    if (!err) {
                        res.status(200).send({ code: 1, data: data });
                    } else {
                        console.error(err);
                        res.status(404).send('系统错误,请稍后再试!');
                    }
                });
                break;

            case 'new':
                if (keyword) where.title = { $regex: keyword };
                iapi.api('av/find', { where: where, other: { skip: now, limit: 20, sort: { _id: -1 }, show: { _id: 12, title: 1, timer: 1, count: 1 } } }, (err, data) => {
                    if (!err) {
                        res.status(200).send({ code: 1, data: data });
                    } else {
                        console.error(err);
                        res.status(404).send('系统错误,请稍后再试!');
                    }
                });
                break;

            default:
                if (keyword) where.title = { $regex: keyword };
                if (req.query.mode && req.query.mode !== '') where.type = req.query.mode;
                console.log(where);
                iapi.api('av/find', { where: where, other: { skip: now, limit: 20, sort: { _id: -1 }, show: { _id: 12, title: 1, timer: 1, count: 1 } } }, (err, data) => {
                    if (!err) {
                        res.status(200).send({ code: 1, data: data });
                    } else {
                        console.error(err);
                        res.status(404).send('系统错误,请稍后再试!');
                    }
                });
                break;
        }
    }
};