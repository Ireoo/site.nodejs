require('./init');
const express = require('express');
const router = express.Router();
const iapi = require('iapi');
const config = require('../config');
const md5 = require('md5');

iapi.config = config.db;
router.data = {};

exports = module.exports = router;

/**
 * 用户注册
 */
router.get('/', (req, res, next) => {
    res.render('reg', { site: config.site, init: { uri: req.query.uri }, user: null });
});

router.post('/', (req, res, next) => {
    let user = req.body;
    let uri = user.uri;
    delete user.uri;
    if (md5(user.password1) == md5(user.password)) {
        delete user.password1;
        user.password = md5(user.password);
        user.createTime = (new Date()).getTime();
        iapi.api('user/insert', { data: user }, (err, data) => {
            console.log(err, data);
            if (err) {
                res.render('reg', { site: config.site, init: { success: false, err: '注册失败，请稍后再试!' }, user: null });
            } else {
                if ("code" in data) {
                    res.render('reg', { site: config.site, init: { success: false, err: '用户名已经被注册!' }, user: null });
                } else {
                    req.session.userId = data._id;
                    delete data.password;
                    // res.render('reg', { site: config.site, init: { success: true }, user: data });
                    (uri && uri !== '') ? res.redirect(301, uri): res.redirect(301, '/');
                }
            }
        });
    }
});
