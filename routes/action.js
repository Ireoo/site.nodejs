require('./init');
const express = require('express');
const router = express.Router();
const iapi = require('iapi');
const config = require('../config');
const md5 = require('md5');

iapi.config = config.db;
router.data = {};

exports = module.exports = router;

router.all('/:key', (req, res, next) => {
    // console.log(req.params.key);
    if (req.params.key && req.params.key !== '') {
        require('./action/' + req.params.key)(req, res, next);
    } else {
        console.error(req.params.key, '不存在!');
        res.status(404).send('NOT FOUND!');
    }
});