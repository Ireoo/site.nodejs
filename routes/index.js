require('./init');
const express = require('express');
const router = express.Router();
const iapi = require('iapi');
const config = require('../config');
const md5 = require('md5');

iapi.config = config.db;
router.data = {};

exports = module.exports = router;

/**
 * 首页内容
 */
router.all('/', (req, res, next) => {
    let where = { close: { $exists: false } };
    let sort = {};
    if (req.query.k) where.title = { $regex: req.query.k };
    switch (req.query.mode) {
        case 'hot':
            sort.count = -1;
            sort._id = -1;
            break;
        default:
            sort._id = -1;
            break;
    }
    if (req.query.mode !== 'new' && req.query.mode !== 'hot') where.type = req.query.mode;
    if (req.query.tag) where.tags = { $in: [req.query.tag] };
    iapi.api(config.db.table + '/find', { where: where, other: { limit: 20, sort: sort, show: { _id: 1, title: 1, timer: 1, count: 1 } } }, (err, data) => {
        if (!err) {
            router.data.list = data;
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all('/', (req, res, next) => {
    router.data.count = {};
    iapi.api(config.db.table + '/count', { where: { type: 'torrent', close: { $exists: false } } }, (err, data) => {
        if (!err) {
            router.data.count.torrent = data;
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all('/', (req, res, next) => {
    iapi.api(config.db.table + '/count', { where: { type: 'image', close: { $exists: false } } }, (err, data) => {
        if (!err) {
            router.data.count.image = data;
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all('/', (req, res, next) => {
    iapi.api(config.db.table + '/count', { where: { type: 'video', close: { $exists: false } } }, (err, data) => {
        if (!err) {
            router.data.count.video = data;
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all('/', (req, res, next) => {
    iapi.api(config.db.table + '/count', { where: { close: { $exists: false } } }, (err, data) => {
        if (!err) {
            router.data.count.top = data;
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all('/', (req, res, next) => {
    if (req.query.mode) {
        iapi.api('tag/find', { where: { type: req.query.mode } }, (err, data) => {
            if (!err) {
                router.tags = data;
                next();
            } else {
                console.error(err);
                res.status(404).send('系统错误,请稍后再试!');
            }
        });
    } else {
        next();
    }
});

router.all('/', (req, res, next) => {
    res.render('index', { site: config.site, init: { mode: req.query.mode ? req.query.mode : 'new', k: req.query.k ? req.query.k : '', menu: config.menu, tag: req.query.tag ? req.query.tag : '', tags: router.tags || [] }, data: router.data, user: req.user });
});