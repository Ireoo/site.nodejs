require('./init');
const express = require('express');
const router = express.Router();
const iapi = require('iapi');
const config = require('../config');
const md5 = require('md5');

iapi.config = config.db;
router.data = {};

exports = module.exports = router;




router.all(/^\/([a-z0-9]{24,})$/, (req, res, next) => {
    res.render('user', { site: config.site, init: {}, data: router.data, user: req.user });
});
