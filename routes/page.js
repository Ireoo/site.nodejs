require('./init');
const express = require('express');
const router = express.Router();
const iapi = require('iapi');
const config = require('../config');
const md5 = require('md5');

iapi.config = config.db;
router.data = {};

exports = module.exports = router;

/**
 * 文章内容
 */
router.all(/^\/([a-z0-9]{24,})$/, (req, res, next) => {
    iapi.api(config.db.table + '/update', { where: { _id: req.params[0] }, data: { $inc: { count: 1 } }, other: { upsert: false } }, (err, data) => {
        if (!err) {
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all(/^\/([a-z0-9]{24,})$/, (req, res, next) => {
    iapi.api(config.db.table + '/findone', { where: { _id: req.params[0] } }, (err, data) => {
        if (!err) {
            data.images.unique();
            router.data = data;
            next();
        } else {
            console.error(err);
            res.status(404).send('系统错误,请稍后再试!');
        }
    });
});

router.all(/^\/([a-z0-9]{24,})$/, (req, res, next) => {
    res.render('page', { site: config.site, init: {}, data: router.data, user: req.user });
});