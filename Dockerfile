FROM node:onbuild

# RUN npm update
RUN apt update && apt upgrade -y && apt dist-upgrade -y && apt install vim -y && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

EXPOSE 80