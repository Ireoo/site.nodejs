process.env.TZ = "Asia/Shanghai";
const express = require('express');
const session = require('express-session');
const ejs = require('ejs');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const routes = require('./routes');
const logger = morgan('[:date[iso]] :remote-addr[:remote-user] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms');
const config = require('./config');
const compress = require('compression');
const md5 = require('md5');
const iapi = require('iapi');

iapi.config = config.db;

// ejs.delimiter = '?';

/**
 * 显示访问信息
 */
app.use(logger);


/**
 * 获取数据流并保存在req.input里面
 */
app.use((req, res, next) => {
    let reqData = [];
    let size = 0;
    req.on('data', (data) => {
        // console.log('>>>req on');
        reqData.push(data);
        size += data.length;
    });
    req.on('end', () => {
        // console.log('>>>req end');
        req.input = Buffer.concat(reqData, size);
    });
    next();
});


/**
 * 处理数据流成POST数据
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/**
 * 设置默认网页路径，并设置网页后缀
 */
app.use(compress());
app.set('views', require('path').join(__dirname, 'theme'));
app.engine('html', ejs.__express);
app.set('view engine', 'ejs');


/**
 * 设置include文件的路径
 */
app.use(express.static(__dirname + '/static'));

/**
 * 设置session
 */
app.use(session({
    rolling: true,
    secret: md5((new Date()).getTime()),
    name: 'siteNodeJS', //这里的name值得是cookie的name，默认cookie的name是：connect.sid
    cookie: { maxAge: 60 * 1000 * 15 }, //设置maxAge是80000ms，即80s后session和相应的cookie失效过期
    resave: true,
    saveUninitialized: true,
}));

/**
 * 全局处理，比如验证key等信息
 */
app.use((req, res, next) => {
    res.setHeader('Server', 'AV');
    if (('domain' in config.init) && config.init.domain !== '') {
        if (req.headers.host === (config.init.domain || '127.0.0.1')) {
            next();
        } else {
            res.status(404).send('NOT FOUND!!!');
        }
    } else {
        next();
    }
    //     console.log('%s http://%s%s', req.method, req.headers.host, req.url);
    //     console.log(req.headers);
    //     console.log(basic.getIP(req));
    //     next();
});

/**
 * 用户是否登陆
 */
app.use(/^\/(.*?)$/, (req, res, next) => {
    if (req.params[0] !== 'login' && req.params[0] !== 'reg' && req.params[0].split('/')[0] !== 'action') {
        if ('userId' in req.session && req.session.userId != '') {
            iapi.api('user/findone', { where: { _id: req.session.userId } }, (err, data) => {
                if (!err && data !== {}) {
                    delete data.password;
                    req.user = data;
                }
                next();
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

/**
 * 路由规则
 */
app.use('/', require('./routes/index'));
app.use('/', require('./routes/page'));
app.use('/login', require('./routes/login'));
app.use('/reg', require('./routes/reg'));
app.use('/action', require('./routes/action'));
app.use('/user', require('./routes/user'));
app.use('/pay', require('./routes/pay'));
app.use('/logout', require('./routes/logout'));



const server = app.listen(config.init.port || 2012, () => {
    console.log(`Listening on port ${server.address().address}:${server.address().port}`);
});